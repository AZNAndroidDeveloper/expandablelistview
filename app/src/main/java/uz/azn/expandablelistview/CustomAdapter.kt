package uz.azn.expandablelistview

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.TextView

class CustomAdapter(
    val context: Context,
    val childList: MutableList<MutableList<String>>,
    val groupList: MutableList<String>
) : BaseExpandableListAdapter() {
    override fun getGroup(groupPosition: Int): String = groupList[groupPosition]

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean = true

    override fun hasStableIds(): Boolean = false

    override fun getGroupView(
        groupPosition: Int,
        isExpanded: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View {

        var convertView = convertView
        if (convertView == null) {
            val layoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.group_items, null)

        }
        val textview = convertView!!.findViewById<TextView>(R.id.group_name)
        textview.text = getGroup(groupPosition).toString()
        return convertView

    }

    override fun getChildrenCount(groupPosition: Int): Int {
//       if(childList.size== null){
//           var convertView  = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//           val inflate = convertView.inflate(R.layout.activity_main, null)
//           val expandale = inflate.findViewById<ExpandableListView>(R.id.expandable_list_view)
//
//
//       }

        return  childList[groupPosition].size
    }

    override fun getChild(groupPosition: Int, childPosition: Int): String =
        childList[groupPosition][childPosition]

    override fun getGroupId(groupPosition: Int): Long = groupPosition.toLong()

    override fun getChildView(
        groupPosition: Int,
        childPosition: Int,
        isLastChild: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View {
        var convertView = convertView
        if (convertView == null) {
            val layoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.child_items, null)
        }
        val childTextView = convertView!!.findViewById<TextView>(R.id.child_text_view)
        childTextView.text = getChild(groupPosition, childPosition)
        return convertView
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long = childPosition.toLong()

    override fun getGroupCount(): Int = groupList.size

}
package uz.azn.expandablelistview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import uz.azn.expandablelistview.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val groupList: MutableList<String> = arrayListOf()
        groupList.add("Toshkent")
        groupList.add("Namangan")
        groupList.add("Andijon")
        groupList.add("Fargona")
        groupList.add("Sirdaryo")
        groupList.add("Qashqadaryo")
        groupList.add("Surxondaryo")
        groupList.add("Xorazm")
        groupList.add("Qoraqolpogiston")
        groupList.add("Buxoro")
        groupList.add("Samarqand")
        val toshkent: MutableList<String> = arrayListOf()
        toshkent.add("Yunusobod")
        toshkent.add("Mirzo Ulugbek")
        toshkent.add("Yashnabot")
        toshkent.add("Sergili")
        toshkent.add("Uchtepa")
        toshkent.add("Chilonzor")
        toshkent.add("Qorasuv")
        val namangan: MutableList<String> = arrayListOf()
        namangan.add("Pop")
        namangan.add("Chust")
        namangan.add("Mingbuloq")
        namangan.add("To'raqorgon")
        namangan.add("Chortoq")
        namangan.add("Yangiqorgon")
        namangan.add("Namangan")
        namangan.add("Uychi")
        namangan.add("Uchqurgon")

        val childList: MutableList<MutableList<String>> = arrayListOf()
        childList.add(toshkent)
        childList.add(namangan)
        val expand = CustomAdapter(this,childList,groupList)
        binding.expandableListView.setAdapter(expand)
    }
}